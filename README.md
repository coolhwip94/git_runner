# Git Runner Setup
> Example files to setup a git runner

- config.toml
    - placed in `/srv/gitlab-runner/config/` directory

- docker-compose.yml
    - containers info for running gitlab runner within docker container

### Running
```
docker-compose up -d
```
